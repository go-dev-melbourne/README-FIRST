![GoDev Melbourne](https://gitlab.com/uploads/user/avatar/954083/avatar.png)


# Brief Overview of Projects

Below is a list of Go projects that date back to 2014 to the present.

Pretty much all of them are "web-based" systems that use a Go server at the 
backend, with some form of Database, and a web based front end built using the 
usual tools.

I like using Go, but Im always happy to jump from tool to tool depending on the 
job. 

# Small Utility Programs

A few small util programs in the repo - useful for me, but nothing really worth
publishing.

Have a browse through 
https://gitlab.com/go-dev-melbourne

Which contains everything Go-related at my end.

# Vendored forks of Open Source projects

I maintain a vendored fork of Gopherjs, a transpiler from Go to JS.

This has been a long term project, and there are much better minds than mine
that are driving the actual development. My contribution to this
project has been a very minor one - mostly working on bug fixes on particular
edge conditions that have got in the way.

Making it work on Windows, and some patches to get it working well on FreeBSD.

See http://gopherjs.org for more details.

What this transpiler actually does is quite clever, and working on the code 
is a great learning experience in what Go is actually capable of, over and 
above being a nice clean language to hack together some web sites with. 

Writing a compiler to JS is also a great way to learn more about JS internals,
and find out how little you really know about JS itself.

Gopherjs uses the standard Go compiler to produce an AST from your front end Go code,
and from that tree it generates the equivalent JS. This part is ridiculously 
simple (https://github.com/gopherjs/gopherjs/blob/master/compiler/compiler.go)

It gets a bit harder from there, as it then dives deep into JS in order to 
portably replicate the behaviour of goroutines without resorting to web workers,
amongst other things.


# Migration to using Go at the Front End  (transpiled to JS)

Over time, you can see the development of my own web based projects to transition
to using "iosmorphic Go" .. ie, writing the front end in Go and transpiling
to JS.

This has a few benefits, and a few downsides. (ie - very large generated JS files)

Whilst I wouldn't recommend doing this in general for a web project, there are 
some situations where it does makes sense. 

In particular :

- Large SPA
- Intranet apps, where latency and bandwidth are non issues
- Database / forms apps, where using a single front/back codebase removes all of the myriad cases of data conversion, or the need for an ORM.
- Opportunity to apply concepts outside of the standard HTTP/JSON framework. ie - websockets, WebRTC, etc.

In the case of my own projects, you can see where this has developed, so I 
will quickly run through these in chrono order :

## 2014  https://gitlab.com/go-dev-melbourne/ActionFront

Uses a Go backend on martini

Tiedot database  (sort of like Redis)

Angular 1 front end

Websocket notifications

## 2015 https://gitlab.com/go-dev-melbourne/itrak.mmaint

Prototype layout design for a proposed factory maint project, with no specs.

Simple Go backend, prototype front end straight JS / bit of Angular / Foundation.

Used this as an exersize to generate requirements for the actual system.

## 2015 https://gitlab.com/go-dev-melbourne/cmms

1st pass :

Go backend, using echo  (https://echo.labstack.com/)

PostgreSQL via DAT (https://github.com/mgutz/dat)

Angular 1 front end, using Material Design CSS

via LumX (http://ui.lumapps.com/)

## 2016 https://gitlab.com/go-dev-melbourne/go-cmms

Significant requirements changes to the above project, re-implemented in 
Gopherjs to get it over the line.

Go backend, echo + DAT libs.

Since the data models that the app was based on were changing significantly
every week (entire new tables / relationships between table changing), 
used Gopherjs to enable a central repo of data definitions, and guarantee
that models would be the same from DB to backend to frontend with each change
(enforced by compiler in all cases)

Change of primary platform from Chrome/IE to iPad = write new CSS lib
based on milligram (http://milligram.github.io/) 
and gridforms (http://kumailht.com/gridforms/)

Requirements for realtime updates to screens and ipads as alerts are raised
or tasks updated.  Changed comms from HTTP to websocket, allowing 
bidirectional updates.

System went live across multiple sites in Aust, and USA, around mid-2016, and
has been running ever since.

Demo of a slightly modified version of this app is availble to play with
at https://cmms.ministryofcolour.net

U: go-melb
P: go-melb

... you can safely raise tasks, take down machines, etc, at least enough to 
get a feel for what the system does.

## Go-CMMS Project Termination

Had an unusual situation with the way that the previous project was managed.

During this project, I was an external contractor with the Engineering
Division, contracted to do all the usual BA and Requirements studies inhouse,
and then develop this system offsite.

The client lacked any internal staff within the division to tackle any of
this sort of work. Large company, with the usual setup of disparate Access 
databases, lots of systems implemented only as Spreadsheets, islands of 
knowledge, and even a PICK system (late 80s) holding all of their critical 
business data.

They did however have a really good web dev team under the same roof which 
looked after their external web site, albeit not as part of the Engineering 
Division.

Even as far as accessing internal web servers, dev environments, git repos, 
NginX proxies, IMAP and SMS gateways, DNS, Atlassian Jira, Photography, etc 
.. (they use everything a modern web dev house uses) 
... the setup and maintenace of pretty much everything that could be considered
"not MS standard" was delegated to the web development team.

So naturally, I worked closely with their web-dev team to setup and build this
system. No other options were available. We had a great working relationship, 
shared a lot of code, problems and solutions. 

I thoroughly enjoyed working with these guys, and would work with them again 
any day on any project. Totally competent.

Unfortunately, the Engineering Division wanted to pull projects back under their 
own control, away from web development. To this end, they decided to re-implement
everything over again, from scratch, using .NET and C#, using full time employees.

Not having expertise in house to do any of this work, they have recently began
agressively recruiting for Microsoft developers to build up a new development 
team.

There are some good opportunities there for the right sort of person, but its
not the space that I need to be in.


## 2016 https://gitlab.com/go-dev-melbourne/formulate

Small library that I use on GopherJS forms based apps, to render HTML CRUD form 
templates on the fly based on data descriptions, and sensibly tie these back to
consistent callbacks.

The code is messy, but it does one job well in my case. 

Example of use - if you have a quick flick through this file

https://gitlab.com/go-dev-melbourne/go-cmms/blob/master/app/machine.go

.. you will see what Im trying to achieve there.

1 lib for rendering lists / add forms / edit forms based on definitions, and ties
this back to consistent callbacks.

Similar to formlyJS, but integrated into an all-Go framework.

## 2016  https://gitlab.com/go-dev-melbourne/fogofwar

Long term project back on the boil.

Quick Re-write of "ActionFront" to use the latest and greatest toys.

Go backend with echo / DAT

Gopherjs frontend using formulate, websockets, Go - RPC, and dynamic SVG UX 
elements.

There is some deep code in this one at the backend, but its a bit hard to 
grok unless you spend a lot of time reading it.

The system allows players to create a world, and then invite other players into
each world to "play a gaming session".  

So there is an engine at the backend that spawns a goroutine for each gaming session,
who's purpose is to manage world state for that instance of a game.

Its probably the only place in all of my repos where I have a valid use case 
to employ the full range of Go features, from long-lived Goroutines, Channels, 
and shared states. 

Mostly in this file :
https://gitlab.com/go-dev-melbourne/fogofwar/blob/master/server/play-goroutine.go

... there is not much code in that file, but it is hard to follow what is going on.

You can have a quick play with the app itself, and get a feel for what all that
code is doing, and how parts of the SVG UI currently behave by going to :

http://wargaming.io
U: test
P: tt

.. its a pretty domain specific app, so not much of it will make sense unless
you are into Napoleon, or the adventures of Maj. Sharpe. Have fun.


## 2017 https://gitlab.com/go-dev-melbourne/jass

Prototyping a new fashion / perfume / clothing brand that is being Launched 
shortly.

Similar setup to the rest :
Go / echo / DAT backend
GopherJS frontend, with jQuery, heavily optimized to reduce the payload issue, 
and make it acceptable for a high volume web site.

99% of this project has been about learning and applying CSS and design. Not my
strong suite, but its been a good learning curve for me.

Anticipated audience is likely to be 99% from high end smartphones - so please
have a look at it from your iPhone 6 or better as well as a big screen PC.

You can access the partially completed project in the staging area here :

https://jass.com.au

... Still a bit to go on this one obviously, adding in a basic set of e-commerce
features, connect it to stripe account to take payments, and then add a simple 
backend to track and dispatch orders.

Code for the admin account is in a sister repo here :

https://gitlab.com/go-dev-melbourne/jass-admin


# Test Cases

I know what I have done very wrong with all this code above - where are the test 
cases ?  They are not there.

I generally rely on external test jigs to commit to testing, and follow
individual scenarios manually. Old habit from doing lots of test-heavy defence
projects in the past, and debugging old modems and serial comms issues.

I know that I should lean on Go's built in test tools a lot more than I do, because
they are very complete, and fit in well with modern CI workflows.

I still think that testing needs to be done by hand, but CI is a big benefit too.

Will improve in this area.

# GitLab permissions

Note to self - If you create a GROUP on gitlab, and add projects to it, and 
make sure that each project is set to PUBLIC, then you still have a problem in
that they cant view the code.

Go back into each project, and set

Feature Visibility -> Repository 
   Then select "Everyone with Access"
   
To test, make sure you logout of gitlab completely (or fire up incognito)
and then connect to gitlab as a different user, and then test each link
to make sure that the files are available.

Apologies for the inconvenience on that one  !!















